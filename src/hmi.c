#include <avr/pgmspace.h>
#include "hmi.h"

const char cons_start[] PROGMEM = "Console Starting";
const char student_name[] PROGMEM = "Sander Ratassepp";

const char ver_fw[] PROGMEM = VER_FW;
const char ver_libc[] PROGMEM = VER_LIBC;

const char m1[] PROGMEM = "January";
const char m2[] PROGMEM = "February";
const char m3[] PROGMEM = "March";
const char m4[] PROGMEM = "April";
const char m5[] PROGMEM = "May";
const char m6[] PROGMEM = "June";
const char m7[] PROGMEM = "July";
const char m8[] PROGMEM = "August";
const char m9[] PROGMEM = "September";
const char m10[] PROGMEM = "October";
const char m11[] PROGMEM = "November";
const char m12[] PROGMEM = "December";

PGM_P const name_month[NAME_MONTH_COUNT] PROGMEM = {
    m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12
};

const char string_1[] PROGMEM = "    _______";
const char string_2[] PROGMEM = "   /      /,";
const char string_3[] PROGMEM = "  /      //";
const char string_4[] PROGMEM = " /______//";
const char string_5[] PROGMEM = "(______(/";
 
PGM_P const banner[BANNER_ROWS] PROGMEM = {
    string_1, string_2, string_3, string_4, string_5

};
