#ifndef PRINT_HELPER_H
#define PRINT_HELPER_H
#include <stdio.h>

void print_banner_P(void (*puts_P_function)(const char*), 
					const char * const *banner, 
					const uint8_t rows);

void print_bytes (const unsigned char *array, const size_t len);
char *bin2hex(unsigned char *p, int len);
#endif /* PRINT_HELPER_H */
