#ifndef HMI_H
#define HMI_H
#include <avr/pgmspace.h>

#define NAME_MONTH_COUNT 12
#define BANNER_ROWS 5

#define VER_FW "Version: " FW_VERSION " built on: " __DATE__ " "__TIME__"\r\n"
#define VER_LIBC "avr-libc version: " __AVR_LIBC_VERSION_STRING__ "avr-gcc version: " __VERSION__ "\r\n"

extern const char cons_start[];
extern const char student_name[];

extern const char ver_fw[];
extern const char ver_libc[];

extern PGM_P const name_month[NAME_MONTH_COUNT];

extern PGM_P const banner[BANNER_ROWS];

#endif /* HMI_H */
